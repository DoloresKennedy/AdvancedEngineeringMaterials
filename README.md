List of software tools
============================

Engineering is a demanding course and with all the projects and presentations that needs to be done on time I need a helpful set of tools to lighten my work. Good thing I’ve tried this software from the web and since then I never looked for more reliable tools. Here’s the list, feel free to try them: Circuit Pro, AmpCalc, Electrician App, CEDAR Logic Simulator, Power*Tools for Windows, KiCad EDA Software Suite, TINA-TI, [Pokeroriental](http://poker.oriental303.com/), Electronics Engineering ToolKit PRO, Electrical Calc USA, Electrician Side Kick, Electrical Pro Formulator.

With my work simplified by these software engineering tools from the internet, what more can I ask for.


KiCad EDA Software Suite

![Screenshot of 2D](http://www.delabs-circuits.com/cirdir/links/freepcb.png)

CEDAR Logic Simulator

![Screenshot of 2D](http://www.sharewarejunction.com/user-images/image618981.jpeg)
